package com.nycschool.domain.usecase.getschools

import com.nycschool.domain.model.School
import com.nycschool.utils.Resource
import io.mockk.every
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetSchoolListUseCaseTest() {
    private val getSchoolListUseCase: GetSchoolListUseCase = mockk(relaxed = true)
    private val data = listOf(
        School(
            dbn = "01M292",
            schoolName = "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
            numOfSatTestTakers = "29",
            satCriticalReadingAvgScore = "355",
            satWritingAvgScore = "363",
            satMathAvgScore = "404"
        )
    )

    @Before
    fun setup() {
        every { getSchoolListUseCase.invoke() } returns flowOf(Resource(data))
    }

    @Test
    fun `school list validation`() {
        runBlocking {
            val actual = getSchoolListUseCase().onEach { result ->
                result.data
            }.flatMapConcat { it.data!!.asFlow() }.toList()
            assertEquals(actual, data)
        }
    }
}