package com.nycschool.domain.usecase.getSchoolDetails

import com.nycschool.domain.model.School
import com.nycschool.domain.model.SchoolDetail
import com.nycschool.domain.usecase.getschools.GetSchoolListUseCase
import com.nycschool.utils.Resource
import io.mockk.every
import io.mockk.mockk
import junit.framework.TestCase
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class GetSchoolDetailUseCaseTest(){

    private val getSchoolDetailUseCase: GetSchoolDetailUseCase = mockk(relaxed = true)
    private val data = listOf(
        SchoolDetail(
            dbn = "02M438",
            schoolName = "International High School at Union Square",
            overviewParagraph = "The mission of International High School at Union Square is to prepare our multicultural student population of recent immigrants to embark on individualized pathways towards skill development, graduation, college, and careers. Our students are immersed in experiential, interdisciplinary projects and English language instruction in all content areas to facilitate their language development and increase content knowledge within a supportive, personalized atmosphere. Drawing upon diversity as a driving force, we empower our students to collaborate, advocate for themselves and others, and identify as global citizens.",
            phoneNumber = "212-533-2560",
            faxNumber = "212-228-2946",
            website = "www.ihs-us.org",
            schoolEmail = "Vramsuchit@schools.nyc.gov"
        )
    )

    @Before
    fun setup() {
        every { getSchoolDetailUseCase.invoke("02M438") } returns flowOf(Resource(data))
    }

    @Test
    fun `school Data validation`() {
        runBlocking {
            val actual = getSchoolDetailUseCase("02M438").onEach { result ->
                result.data
            }.flatMapConcat { it.data!!.asFlow() }.toList()
            TestCase.assertEquals(actual, data)
        }
    }

}