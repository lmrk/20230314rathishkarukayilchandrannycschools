package com.nycschool.presentation.school_list.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.nycschool.domain.model.School

@Composable
fun SchoolListItem(
    school: School,
    onItemClick: (School) -> Unit
){
    Row(modifier = Modifier
        .fillMaxWidth()
        .clickable { onItemClick(school) }
        .padding(20.dp),
    horizontalArrangement = Arrangement.SpaceBetween) {
        Text(text = "${school.schoolName}",
        style = MaterialTheme.typography.body1,
            overflow = TextOverflow.Ellipsis
        )
    }
}