package com.nycschool.presentation.school_list

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyHorizontalGrid
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.interview.a20230314_rathishkarukayilchandran_nycschools.R
import com.nycschool.domain.model.School
import com.nycschool.presentation.Screen
import com.nycschool.presentation.ui.theme.*


@Composable
fun SchoolListScreen(
    navController: NavController,
    viewModel: SchoolListViewModel = hiltViewModel()
) {
    // Fetching current app configuration
    val configuration = LocalConfiguration.current

    // When orientation is Landscape
    when (configuration.orientation) {
        //TODO: Design for Landscape
        Configuration.ORIENTATION_LANDSCAPE -> {
            val state = viewModel.state.value
            Column() {
                Row {
                   GreetingSection()
                    ChipSection(
                        chips = listOf(
                            "SAT MATH",
                            "SAT READING",
                            "SAT WRITING"
                        )
                    )

                }
                Column() {
                    LazyHorizontalGrid(
                        rows = GridCells.Fixed(1),
                        contentPadding = PaddingValues(start = 7.5.dp, end = 7.5.dp, bottom = 100.dp),
                        modifier = Modifier.fillMaxHeight()
                    ) {
                        items(state.schoolList.size) {
                            SchoolItem(school = state.schoolList[it], navController)
                        }
                    }

                }
            }
        }

        // For Portrait Mode
        else -> {
            val state = viewModel.state.value
            Box(modifier = Modifier.fillMaxSize()) {
                Column() {
                    GreetingSection()
                    //TODO:Filter the list based on SAT Scores
                    ChipSection(
                        chips = listOf(
                            "SAT MATH",
                            "SAT READING",
                            "SAT WRITING"
                        )
                    )
                    if(state.schoolList.isNotEmpty()){
                        SchoolSection(schools = state.schoolList, navController )
                    }
                    if (state.error.isNotBlank()) {
                        Text(
                            text = state.error,
                            color = MaterialTheme.colors.error,
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 20.dp)
                        )
                    }
            if(state.isLoading) {
                CircularProgressIndicator()
            }
                }
            }
        }
    }

}

@Composable
fun GreetingSection() {
    Row(
        modifier = Modifier
            .padding(15.dp)
    ) {
        Column(
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = "Good morning,",
                style = MaterialTheme.typography.h4
            )
            Text(
                text = "NewYork School Lists!",
                style = MaterialTheme.typography.h5
            )
        }
        Icon(
            painter = painterResource(id = R.drawable.ic_search),
            contentDescription = "Search",
            tint = Color.White,
            modifier = Modifier.size(50.dp)
        )
    }
}

@Composable
    fun ChipSection(chips: List<String>) {
        var selectedChipIndex by remember {
            mutableStateOf(0)
        }
         Text(
            text = "Filters",
            fontWeight = FontWeight.Bold,
            style = MaterialTheme.typography.h5,
            modifier = Modifier.padding(15.dp)
        )
        LazyRow {
            items(chips.size) {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .padding(start = 15.dp, top = 15.dp, bottom = 15.dp)
                        .clickable {
                            selectedChipIndex = it
                        }
                        .clip(RoundedCornerShape(10.dp))
                        .background(
                            if (selectedChipIndex == it) Purple200
                            else Purple500
                        )
                        .padding(15.dp)
                ) {
                    Text(text = chips[it], color = TextWhite)
                }
            }
        }
    }

    @Composable
    fun SchoolSection(schools: List<School>,
    navController: NavController) {
        Column(modifier = Modifier.fillMaxWidth()) {
            Text(
                text = "Schools",
                fontWeight = FontWeight.Bold,
                style = MaterialTheme.typography.h5,
                modifier = Modifier.padding(15.dp)
            )
            LazyVerticalGrid(
                columns = GridCells.Fixed(2),
                contentPadding = PaddingValues(start = 7.5.dp, end = 7.5.dp, bottom = 100.dp),
                modifier = Modifier.fillMaxHeight()
            ) {
                items(schools.size) {
                    SchoolItem(school = schools[it], navController)
                }
            }
        }
    }

    @Composable
    fun SchoolItem(
        school: School,
        navController: NavController
    ) {
        val brush = Brush.verticalGradient(listOf(BabyBlue, DarkBabyBlue))
        BoxWithConstraints(
            modifier = Modifier
                .padding(7.5.dp)
                .aspectRatio(1f)
                .clip(RoundedCornerShape(10.dp))

        ) {
            val width = constraints.maxWidth
            val height = constraints.maxHeight

            Box(
                modifier = Modifier
                    .clickable {
                        navController.navigate(Screen.SchoolDetailScreen.route + "/${school.dbn}")
                    }
                    .background(brush)
                    .fillMaxSize()
                    .padding(15.dp)

            ) {
                Text(
                    text = school.schoolName,
                    maxLines = 3,
                    overflow = TextOverflow.Ellipsis,
                    color = TextWhite,
                    style = MaterialTheme.typography.subtitle1,
                    lineHeight = 26.sp,
                    modifier = Modifier.align(Alignment.TopStart)
                )

                Spacer(
                    modifier = Modifier
                        .height(10.dp)
                        .fillMaxHeight()
                )
                Row( Modifier.align(Alignment.BottomCenter)) {
                    Text(
                        text = "MATH \n  ${school.satMathAvgScore}",
                        color = Purple700,
                        fontWeight = FontWeight.Bold,
                        fontSize = 12.sp)

                    Spacer(
                        modifier = Modifier
                            .width(8.dp)
                            .fillMaxWidth()
                    )

                    Text(
                        text = "READ \n  ${school.satCriticalReadingAvgScore}",
                        color = Purple700,
                        fontWeight = FontWeight.Bold,
                        fontSize = 12.sp)


                    Spacer(
                        modifier = Modifier
                            .width(8.dp)
                            .fillMaxWidth()
                    )

                    Text(
                        text = "WRITE \n  ${school.satWritingAvgScore}",
                        color = Purple700,
                        fontWeight = FontWeight.Bold,
                        fontSize = 12.sp)
                }
            }
        }
}
