package com.nycschool.presentation

sealed class Screen(val route: String) {
    object SchoolListScreen: Screen("school_list_screen")
    object SchoolDetailScreen: Screen("school_detail_screen")
}