package com.nycschool.presentation.school_detail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Call
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.*
import androidx.hilt.navigation.compose.hiltViewModel
import com.nycschool.domain.model.SchoolDetail
import com.nycschool.presentation.ui.theme.BabyBlue
import com.nycschool.presentation.ui.theme.DarkBabyBlue


@Composable
fun SchoolDetailScreen(
    viewModel: SchoolDetailViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scrollState = rememberScrollState()

    Column(modifier = Modifier.fillMaxSize()) {
        BoxWithConstraints(modifier = Modifier.weight(1f)) {
            Surface {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .verticalScroll(scrollState),
                ) {
                    if (state.error.isNotBlank()) {
                        Text(
                            text = state.error,
                            color = MaterialTheme.colors.error,
                            textAlign = TextAlign.Center,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 20.dp)
                        )
                    }
                    if (state.schoolDetail != null) {
                        state.schoolDetail?.let {
                            ProfileHeader(
                                scrollState,
                                it,
                                this@BoxWithConstraints.maxHeight
                            )
                        }
                        state.schoolDetail?.let {
                            ProfileContent(
                                it,
                                this@BoxWithConstraints.maxHeight
                            )
                        }
                    }

                }
            }
            performCall(
                modifier = Modifier.align(Alignment.BottomEnd), viewModel
            )
        }
    }
}

@Composable
private fun ProfileHeader(
    scrollState: ScrollState,
    schoolDetail: SchoolDetail,
    containerHeight: Dp
) {
    val offset = (scrollState.value / 2)
    val offsetDp = with(LocalDensity.current) { offset.toDp() }

    Spacer(
        modifier = Modifier
            .height(280.dp)
            .fillMaxWidth()
            .background(Brush.horizontalGradient(listOf(BabyBlue, DarkBabyBlue)))
    )
}

@Composable
private fun ProfileContent(schoolDetail: SchoolDetail, containerHeight: Dp) {
    Column {
        Spacer(modifier = Modifier.height(8.dp))
        Name(schoolDetail)
        val schoolName = Pair("School Name:", schoolDetail.schoolName)
        val schoolDBN = Pair("DBN:", schoolDetail.dbn)
        val schoolWebsite = Pair("Website:", schoolDetail.website)
        val schoolFaxNumber = Pair("Fax Number:", schoolDetail.faxNumber)
        val overview = Pair("Overview:", schoolDetail.overviewParagraph)
        val schoolDetailList =
            listOf(schoolName, schoolDBN, schoolWebsite, schoolFaxNumber, overview)
        schoolDetailList.forEach() {
            ProfileProperty(it.first, it.second)
        }
        Spacer(Modifier.height((containerHeight - 320.dp).coerceAtLeast(0.dp)))
    }
}

@Composable
private fun Name(
    schoolDetail: SchoolDetail
) {
    Column(modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 16.dp)) {
        Name(
            schoolDetail = schoolDetail,
            modifier = Modifier.paddingFromBaseline(20.dp)
        )
    }
}

@Composable
private fun Name(schoolDetail: SchoolDetail, modifier: Modifier = Modifier) {
    Text(
        text = schoolDetail.schoolName,
        modifier = modifier,
        style = MaterialTheme.typography.h5,
        fontWeight = FontWeight.Bold
    )
}

@Composable
fun ProfileProperty(label: String, value: String, isLink: Boolean = false) {
    Column(modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 16.dp)) {
        Divider()
        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
            Text(
                text = label,
                modifier = Modifier.paddingFromBaseline(24.dp),
                style = MaterialTheme.typography.caption,
            )
        }
        val style = if (isLink) {
            MaterialTheme.typography.body1.copy(color = MaterialTheme.colors.primary)
        } else {
            MaterialTheme.typography.body1
        }
        Text(
            text = value,
            modifier = Modifier.paddingFromBaseline(24.dp),
            style = style
        )
    }
}

@Composable
fun performCall(modifier: Modifier = Modifier, viewModel: SchoolDetailViewModel) {
    val context = LocalContext.current
    FloatingActionButton(
        onClick = {
            val phoneNumber = viewModel.state.value.schoolDetail?.phoneNumber
            phoneNumber?.let {
                openDialer(
                    phoneNumber = phoneNumber,
                    context
                )
            }
        },
        modifier = modifier
            .padding(16.dp)
            .padding()
            .height(48.dp)
            .widthIn(min = 48.dp),
        backgroundColor = Color.Blue,
        contentColor = Color.White,

        ) {
        Icon(Icons.Outlined.Call, "")
    }
}


fun openDialer(phoneNumber: String, context: Context) {
    val uri = Uri.parse("tel:$phoneNumber")
    val intent = Intent(Intent.ACTION_DIAL, uri)
    try {
        context.startActivity(intent)
    } catch (ex: SecurityException) {
        Toast.makeText(context, "Unable to open Dialer", Toast.LENGTH_SHORT).show()
    }

}



