package com.nycschool.domain.model

data class School(val dbn: String,
                  val numOfSatTestTakers: String,
                  val satCriticalReadingAvgScore: String,
                  val satMathAvgScore: String,
                  val satWritingAvgScore: String,
                  val schoolName: String)
