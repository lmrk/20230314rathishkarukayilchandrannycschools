package com.nycschool.domain.model


data class SchoolDetail(
    val dbn: String,
    val overviewParagraph: String,
    val phoneNumber: String,
    val faxNumber: String,
    val schoolEmail: String,
    val schoolName: String,
    val website: String
)