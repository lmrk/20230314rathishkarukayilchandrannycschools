package com.nycschool.domain.repository

import com.nycschool.data.remote.dto.SchoolDTO
import com.nycschool.data.remote.dto.SchoolDetailDTO


interface SchoolListRepository {
    suspend fun getSchoolList(): List<SchoolDTO>
    suspend fun getSchoolDetail(dbn: String): List<SchoolDetailDTO>
}