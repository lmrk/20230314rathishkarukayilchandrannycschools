package com.nycschool.domain.usecase.getschools

import com.nycschool.data.remote.dto.toSchool
import com.nycschool.domain.model.School
import com.nycschool.domain.repository.SchoolListRepository
import com.nycschool.utils.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetSchoolListUseCase @Inject constructor(
    private val repository: SchoolListRepository
) {
    operator fun invoke(): Flow<Resource<List<School>>> = flow {
        try {
           val schoolList = repository.getSchoolList().map { it.toSchool() }
            emit(Resource.Success(schoolList))

        }catch (ex: HttpException){
            emit(Resource.Error(ex.localizedMessage ?: "An unexpected error occured"))
        }catch (ex: IOException){
            emit(Resource.Error("Couldn't reach server. Check your internet connection."))
        }
    }
}