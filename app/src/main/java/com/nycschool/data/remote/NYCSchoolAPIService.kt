package com.nycschool.data.remote

import com.nycschool.data.remote.dto.SchoolDTO
import com.nycschool.data.remote.dto.SchoolDetailDTO
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query


interface NYCSchoolAPIService {

    @Headers("Content-Type: application/json")
    @GET("f9bf-2cp4.json")
    suspend fun getSchoolList(): List<SchoolDTO>

    @Headers("Content-Type: application/json")
    @GET("s3k6-pzi2.json")
    suspend fun getSchoolDetail(@Query("dbn") dbn: String): List<SchoolDetailDTO>
}